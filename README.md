# nth-somacube-rs

Soma cube ( https://en.wikipedia.org/wiki/Soma_cube ) を解くプログラムを作成しています。

## 機能

以下の機能を実装する予定です。

- [x] 3x3x3 を解く
- [x] 4x4x4 を解く (5x5x5 が終わらないので、マス数が増えることの影響がどう出るかを見るため追加。)
- [x] 5x5x5 を解く
    - [ ] 現実的な時間で動作するように最適化する。

## 開発

Rust ツールチェインをインストールしてください。

https://www.rust-lang.org/ja/tools/install

また、`cargo install cargo-watch` で `cargo-watch` をインストールしてください。

リポジトリーをクローンした後に `dev.sh` を実行してください。

```sh
git clone git@gitlab.com:nogiro/nth-somacube-rs.git path/to/working
cd $_
```

```sh
./dev.sh
```

## 実行

### 3x3x3 の回答を 1 個

```sh
cargo run --release -- -v --count 1
```

### 5x5x5 の回答を 3 個

```sh
cargo run --release -- -v fifth
```
