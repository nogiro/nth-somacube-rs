#!/usr/bin/env bash

set -u

cargo watch \
  -x fmt \
  -x test \
  -x 'doc --no-deps --document-private-items' \
  -x clippy
