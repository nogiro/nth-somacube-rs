use clap::ValueEnum;

#[derive(ValueEnum, Debug, Clone)]
pub enum SizeMode {
    /// 3x3x3 (Soma cube)
    Third,

    /// 4x4x4 and double tetracubes (16 pieces)
    Forth,

    /// 5x5x5 and pentacubes (29 peices)
    Fifth,
}

impl std::fmt::Display for SizeMode {
    fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            Self::Third => "third",
            Self::Forth => "forth",
            Self::Fifth => "fifth",
        }
        .fmt(formatter)
    }
}
