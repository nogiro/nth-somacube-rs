use crate::Algorithm as Ca;
use clap::ValueEnum;

/// ピースをはめ込む空きマスの選出方法
#[derive(ValueEnum, Debug, Clone)]
pub enum Algorithm {
    /// 残りのピース袋から 2 個取り出して 1 個にくっつけて袋に戻すのを繰り返す。
    MergePiece,

    /// [`LvnAndLowVarianceInSpace`][`Algorithm::LvnAndLowVarianceInSpace`] (LVNALVIS) をシャッフルしない版
    LvnalvisNoRandom,

    /// [`LeastVacantNeighbors`][`Algorithm::LeastVacantNeighbors`] (LVN) に加えて配置後それぞれの空きマスの隣接空間数数の分散を減らす
    LvnAndLowVarianceInSpace,

    /// 隣接空きマス数が 1 番少ないものを必ず選ぶ
    LeastVacantNeighbors,

    /// 空きマスからランダムで選ぶ
    Random,

    /// 候補生成後に並べ替えせずそのまま使う
    NoRandom,
}

impl std::fmt::Display for Algorithm {
    fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            Self::MergePiece => "merge-piece",
            Self::LvnalvisNoRandom => "lvnalvis-no-random",
            Self::LvnAndLowVarianceInSpace => "lvn-and-low-variance-in-space",
            Self::LeastVacantNeighbors => "least-vacant-neighbors",
            Self::Random => "random",
            Self::NoRandom => "no-random",
        }
        .fmt(formatter)
    }
}

impl From<Algorithm> for Ca {
    fn from(algo: Algorithm) -> Self {
        match algo {
            Algorithm::MergePiece => Self::MergePiece,
            Algorithm::LvnalvisNoRandom => Self::LvnalvisNoRandom,
            Algorithm::LvnAndLowVarianceInSpace => Self::LvnAndLowVarianceInSpace,
            Algorithm::LeastVacantNeighbors => Self::LeastVacantNeighbors,
            Algorithm::Random => Self::Random,
            Algorithm::NoRandom => Self::NoRandom,
        }
    }
}
