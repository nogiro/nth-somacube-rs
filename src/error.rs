use crate::model::Point;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("InvalidPieceIsEmpty")]
    InvalidPieceIsEmpty,

    #[error("InvalidPieceHasSamePoint: {0:?}")]
    InvalidPieceHasSamePoint(Point),

    #[error("InvalidPieceIsNotSorted")]
    InvalidPieceIsNotSorted,

    #[error("InvalidPieceIsSeperated")]
    InvalidPieceIsSeperated,

    #[error("InvalidFormatToBeConvertedToPiece")]
    InvalidFormatToBeConvertedToPiece,

    #[error("BackTraceIsExhausted")]
    BackTraceIsExhausted,

    #[error("FailedRemovePiece")]
    FailedRemovePiece,

    // 計算ステップにはいらないで成功するなので発生しないはず
    #[error("PointInSpaceIsExhausted")]
    PointInSpaceIsExhausted,

    #[error("PieceIsExhausted")]
    PieceIsExhausted,

    #[error("CandidatesIsExhausted")]
    CandidatesIsExhausted,

    #[error("AnyhowError: {0}")]
    AnyhowError(#[from] anyhow::Error),
}
