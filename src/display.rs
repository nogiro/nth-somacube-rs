use crate::model::{Point, Space};
use std::collections::BTreeSet;

pub enum StateDisplay {
    Space(Space),
}

impl From<Space> for StateDisplay {
    fn from(inner: Space) -> Self {
        Self::Space(inner)
    }
}

impl StateDisplay {
    pub fn fmt(&self) -> Vec<String> {
        match self {
            Self::Space(sp) => Self::fmt_space(sp),
        }
    }

    pub fn fmt_space(space: &Space) -> Vec<String> {
        let mut ret = vec![];
        let mut s = String::new();

        let points: BTreeSet<_> = space
            .placed()
            .iter()
            .cloned()
            .chain(space.iter().map(|point| (point.clone(), &'_').into()))
            .collect();

        let mut px = 0;
        let mut py = 0;
        for (Point { x, y, .. }, label) in points.iter().map(|cell| (cell.point(), cell.label())) {
            if *y != py {
                s += "|";
            }

            if *x != px {
                ret.push(s);
                s = String::new();
            }

            s += &label.to_string();

            px = *x;
            py = *y;
        }
        s += "|";
        ret.push(s);

        ret
    }
}
