use crate::model::{Piece, Point, Position, Space};
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct PieceMetadata {
    pub origin: Arc<Piece>,
    pub enabled_places: Arc<Vec<EnabledPlace>>,
}

#[derive(Debug, Clone)]
pub struct EnabledPlace {
    pub offset: Arc<Point>,
    pub place: Arc<Piece>,
}

impl PieceMetadata {
    pub fn new(piece: Piece, space: &Space) -> Self {
        let p = piece
            .enabled_places(space)
            .into_iter()
            .map(|(offset, place)| EnabledPlace { offset, place })
            .collect();

        Self {
            origin: Arc::new(piece),
            enabled_places: Arc::new(p),
        }
    }
}

impl From<(&PieceMetadata, &EnabledPlace)> for Position {
    fn from((meta, ep): (&PieceMetadata, &EnabledPlace)) -> Self {
        Self {
            piece: ep.place.clone(),
            offset: ep.offset.clone(),
            origin: meta.origin.clone(),
        }
    }
}
