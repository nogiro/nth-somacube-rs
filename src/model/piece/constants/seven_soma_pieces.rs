use crate::model::piece::constants::tetracube_pieces::{
    piece_a, piece_b, piece_l, piece_p, piece_t, piece_z,
};
use crate::model::Piece;

/// <https://en.wikipedia.org/wiki/Soma_cube#Pieces>
pub fn seven_soma_pieces() -> Vec<Piece> {
    vec![
        piece_v(),
        piece_l(),
        piece_t(),
        piece_z(),
        piece_a(),
        piece_b(),
        piece_p(),
    ]
}

fn piece_v() -> Piece {
    let s = r#"
        **|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'v'))
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::Result;

    #[test]
    fn test_validate() -> Result<()> {
        let pieces = seven_soma_pieces();
        assert_eq!(pieces.len(), 7);
        for piece in pieces.into_iter() {
            piece.validate()?;
        }

        Ok(())
    }
}
