use crate::model::Piece;

/// <http://www.recmath.org/PolyCur/c5lset/index.html>
pub fn pentacube_pieces() -> Vec<Piece> {
    vec![
        piece_a(),  // 1
        piece_b(),  // 2
        piece_e(),  // 3
        piece_em(), // 4
        piece_f(),  // 5
        piece_g(),  // 6
        piece_gm(), // 7
        piece_h(),  // 8
        piece_hm(), // 9
        piece_i(),  // 10
        piece_j(),  // 11
        piece_jm(), // 12
        piece_k(),  // 13
        piece_l(),  // 14
        piece_m(),  // 15
        piece_n(),  // 16
        piece_p(),  // 17
        piece_q(),  // 18
        piece_r(),  // 19
        piece_rm(), // 20
        piece_s(),  // 21
        piece_sm(), // 22
        piece_t(),  // 23
        piece_u(),  // 24
        piece_v(),  // 25
        piece_w(),  // 26
        piece_x(),  // 27
        piece_y(),  // 28
        piece_z(),  // 29
    ]
}

fn piece_a() -> Piece {
    let s = r#"
        *-|**
        *-|-*
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'a'))
}

fn piece_b() -> Piece {
    let s = r#"
        --|-*|--
        *-|**|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'b'))
}

fn piece_e() -> Piece {
    let s = r#"
        --|*-|--
        **|*-|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'e'))
}

/// [`piece_e`] の z 軸反転
fn piece_em() -> Piece {
    let s = r#"
        **|*-|*-
        --|*-|--
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'E'))
}

fn piece_f() -> Piece {
    let s = r#"
        -*-|***|*--
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'f'))
}

fn piece_g() -> Piece {
    let s = r#"
        ---|**-
        -**|-*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'g'))
}

/// [`piece_g`] の z 軸反転
fn piece_gm() -> Piece {
    let s = r#"
        -**|-*-
        ---|**-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'G'))
}

fn piece_h() -> Piece {
    let s = r#"
        --|--|*-
        -*|**|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'h'))
}

/// [`piece_h`] の z 軸反転
fn piece_hm() -> Piece {
    let s = r#"
        -*|**|*-
        --|--|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'H'))
}

fn piece_i() -> Piece {
    let s = r#"
        *|*|*|*|*
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'i'))
}

fn piece_j() -> Piece {
    let s = r#"
        -*|--|--
        **|*-|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'j'))
}

/// [`piece_j`] の z 軸反転
fn piece_jm() -> Piece {
    let s = r#"
        **|*-|*-
        -*|--|--
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'J'))
}

fn piece_k() -> Piece {
    let s = r#"
        *-|--|--
        **|*-|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'k'))
}

fn piece_l() -> Piece {
    let s = r#"
        *-|*-|*-|**
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'l'))
}

fn piece_m() -> Piece {
    let s = r#"
        --|*-|--
        *-|**|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'m'))
}

fn piece_n() -> Piece {
    let s = r#"
        -*|**|*-|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'n'))
}

fn piece_p() -> Piece {
    let s = r#"
        **|**|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'p'))
}

fn piece_q() -> Piece {
    let s = r#"
        **|-*
        **|--
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'q'))
}

fn piece_r() -> Piece {
    let s = r#"
        --|*-|--
        -*|**|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'r'))
}

/// [`piece_r`] の z 軸反転
fn piece_rm() -> Piece {
    let s = r#"
        -*|**|*-
        --|*-|--
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'R'))
}

fn piece_s() -> Piece {
    let s = r#"
        --|--|*-
        **|*-|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'s'))
}

/// [`piece_s`] の z 軸反転
fn piece_sm() -> Piece {
    let s = r#"
        **|*-|*-
        --|--|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'S'))
}

fn piece_t() -> Piece {
    let s = r#"
        -|*|-
        -|*|-
        *|*|*
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'t'))
}

fn piece_u() -> Piece {
    let s = r#"
        **|*-|**
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'u'))
}

fn piece_v() -> Piece {
    let s = r#"
        *--|*--|***
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'v'))
}

fn piece_w() -> Piece {
    let s = r#"
        -**|**-|*--
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'w'))
}

fn piece_x() -> Piece {
    let s = r#"
        -*-|***|-*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'x'))
}

fn piece_y() -> Piece {
    let s = r#"
        --*-|****
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'y'))
}

fn piece_z() -> Piece {
    let s = r#"
        --*|***|*--
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'z'))
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::Result;
    use std::collections::HashSet;

    #[test]
    fn test_validate() -> Result<()> {
        let pieces = pentacube_pieces();
        assert_eq!(pieces.len(), 29);
        for piece in pieces.into_iter() {
            piece.validate()?;
        }

        Ok(())
    }

    #[test]
    fn test_unique_label() -> Result<()> {
        let pieces = pentacube_pieces();

        let labels: HashSet<_> = pieces.iter().map(|x| x.label).collect();

        assert_eq!(pieces.len(), labels.len());

        Ok(())
    }
}
