use crate::model::Piece;

/// <https://en.wikipedia.org/wiki/Polycube>
pub fn double_tetracube_pieces() -> Vec<Piece> {
    [
        piece_l(),
        piece_t(),
        piece_z(),
        piece_a(),
        piece_b(),
        piece_p(),
        piece_i(),
        piece_o(),
    ]
    .into_iter()
    .flat_map(|p| [to_uppercase(&p), p])
    .collect()
}

fn to_uppercase(piece: &Piece) -> Piece {
    let label = match *piece.label() {
        'l' => &'L',
        't' => &'T',
        'z' => &'Z',
        'a' => &'A',
        'b' => &'B',
        'p' => &'P',
        'i' => &'I',
        'o' => &'O',
        _ => unreachable!(),
    };

    Piece::from((piece.clone(), label))
}

pub fn piece_l() -> Piece {
    let s = r#"
        ***|*--
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'l'))
}

pub fn piece_t() -> Piece {
    let s = r#"
        ***|-*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'t'))
}

pub fn piece_z() -> Piece {
    let s = r#"
        **-|-**
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'z'))
}

pub fn piece_a() -> Piece {
    let s = r#"
        **|*-
        --|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'a'))
}

pub fn piece_b() -> Piece {
    let s = r#"
        **|-*
        --|-*
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'b'))
}

pub fn piece_p() -> Piece {
    let s = r#"
        *-|**
        --|*-
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'p'))
}

fn piece_i() -> Piece {
    let s = r#"
        ****
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'i'))
}

fn piece_o() -> Piece {
    let s = r#"
        **|**
    "#;
    let mut piece = Piece::try_from(s).unwrap(); // 固定文字列からでかつ、自動テストで try_from できることを確認している
    piece.sort();
    Piece::from((piece, &'o'))
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::Result;

    #[test]
    fn test_validate() -> Result<()> {
        let pieces = double_tetracube_pieces();
        assert_eq!(pieces.len(), 16);
        for piece in pieces.into_iter() {
            piece.validate()?;
        }

        Ok(())
    }
}
