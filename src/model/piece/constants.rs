mod pentacube_pieces;
mod seven_soma_pieces;
mod tetracube_pieces;

pub use pentacube_pieces::pentacube_pieces;
pub use seven_soma_pieces::seven_soma_pieces;
pub use tetracube_pieces::double_tetracube_pieces;
