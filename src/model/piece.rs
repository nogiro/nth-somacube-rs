mod constants;
mod metadata;

pub use crate::model::piece::metadata::PieceMetadata;

use crate::model::{
    AddOffset, LabeledPoint, Point, PointCollection, Rotate, Space, TakableFromSpace,
};
use crate::Error;
use std::sync::Arc;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct PieceLabel(pub &'static char);

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Piece {
    points: Vec<Point>,
    label: PieceLabel,
}

impl Piece {
    /// 3x3x3 向けの [`Piece`] セット
    pub fn seven_soma_pieces() -> Vec<Self> {
        constants::seven_soma_pieces()
    }

    /// 4x4x4 向けの [`Piece`] セット
    pub fn double_tetracube_pieces() -> Vec<Self> {
        constants::double_tetracube_pieces()
    }

    /// 5x5x5 向けの [`Piece`] セット
    pub fn pentacube_pieces() -> Vec<Self> {
        constants::pentacube_pieces()
    }

    pub fn label(&self) -> &'static char {
        self.label.0
    }

    pub fn sort(&mut self) {
        self.points.sort()
    }

    /// 配置可能場所の一覧を得る
    pub fn enabled_places(&self, space: &Space) -> Vec<(Arc<Point>, Arc<Piece>)> {
        let mut ret = vec![];

        let r = self.rotate_set();
        for point in space.iter() {
            let point = Arc::new(point.clone());

            for rotated in r.iter() {
                let point_in_piece = rotated.first();

                let offset = point.difference(point_in_piece);
                let place = rotated.add_offset(&offset);

                if !space.takable(&place) {
                    continue;
                }

                ret.push((point.clone(), Arc::new(place)));
            }
        }

        ret
    }

    pub fn is_empty(&self) -> bool {
        self.points.is_empty()
    }

    pub fn len(&self) -> usize {
        self.points.len()
    }

    pub fn into_point_iter(self) -> std::vec::IntoIter<Point> {
        self.points.into_iter()
    }

    pub fn first(&self) -> &Point {
        self.points.first().unwrap() // constants から作られた Piece は自動テストで 1 個は Point がある
    }

    pub fn has(&self, point: &Point) -> bool {
        self.points.contains(point)
    }
}

impl PointCollection for Piece {
    type PointItem = Point;
    type PointIter<'a> = std::slice::Iter<'a, Point>;

    fn point_iter(&self) -> Self::PointIter<'_> {
        self.points.iter()
    }
    fn map_to_point<'a>(&self, p: &'a Self::PointItem) -> &'a Point {
        p
    }
}

impl TakableFromSpace for Piece {
    fn labeled_point(&self) -> Vec<LabeledPoint> {
        self.clone().into()
    }
}

impl AddOffset for Piece {
    /// 移動する
    fn add_offset(&self, p: &Point) -> Self {
        Self {
            points: self.points.iter().map(|x| x.add_offset(p)).collect(),
            label: self.label,
        }
    }
}

impl Rotate for Piece {
    /// x 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_x(&self, num: u16) -> Self {
        Self {
            points: self.points.iter().map(|p| p.rotate_x(num)).collect(),
            label: self.label,
        }
    }

    /// y 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_y(&self, num: u16) -> Self {
        Self {
            points: self.points.iter().map(|p| p.rotate_y(num)).collect(),
            label: self.label,
        }
    }

    /// z 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_z(&self, num: u16) -> Self {
        Self {
            points: self.points.iter().map(|p| p.rotate_z(num)).collect(),
            label: self.label,
        }
    }
}

impl Default for PieceLabel {
    fn default() -> Self {
        const DEFAULT_LABEL: &char = &'*';
        Self(DEFAULT_LABEL)
    }
}

impl From<&'static char> for PieceLabel {
    fn from(inner: &'static char) -> Self {
        Self(inner)
    }
}

impl From<Vec<Point>> for Piece {
    fn from(points: Vec<Point>) -> Self {
        Self {
            points,
            label: Default::default(),
        }
    }
}

impl From<(Vec<Point>, &'static char)> for Piece {
    fn from((points, label): (Vec<Point>, &'static char)) -> Self {
        Self {
            points,
            label: PieceLabel(label),
        }
    }
}

impl From<(Piece, &'static char)> for Piece {
    fn from((piece, label): (Piece, &'static char)) -> Self {
        Self {
            points: piece.points,
            label: PieceLabel(label),
        }
    }
}

impl TryFrom<&str> for Piece {
    type Error = Error;

    /// 特定のフォーマットの文字列から構築する
    /// ```
    /// # use nth_somacube_rs as sc;
    /// # use sc::model::{Piece, Point};
    /// let p1 = {
    ///     // (0,0,0)(1,0,0)|(0,1,0)(1,1,0)
    ///     // (0,0,1)(1,0,1)|(0,1,1)(1,1,1)
    ///     let s = r#"
    ///         *-|*-
    ///         **|--
    ///     "#;
    ///     Piece::try_from(s).unwrap()
    /// };
    ///
    /// let p2 = {
    ///     let mut points = vec![
    ///         Point::new(0, 0, 0),
    ///         Point::new(0, 0, 1),
    ///         Point::new(0, 1, 0),
    ///         Point::new(1, 0, 1),
    ///     ];
    ///     points.sort();
    ///     Piece::from(points)
    /// };
    ///
    /// assert_eq!(p1, p2);
    /// ```
    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let chars: Vec<Vec<_>> = s
            .split('\n')
            .map(|s1| s1.trim())
            .filter(|s1| !s1.is_empty())
            .map(|s1| s1.split('|').map(|s2| s2.chars()).collect())
            .collect();

        let mut ret = vec![];

        let mut x = 0;
        let mut y = 0;

        for (z, c1) in chars.into_iter().enumerate() {
            for c2 in c1 {
                for c3 in c2 {
                    if c3 == '*' {
                        let z = z.try_into().map_err(|_| Error::InvalidPieceIsSeperated)?;
                        ret.push(Point::new(x, y, z));
                    } else if c3 != '-' {
                        return Err(Error::InvalidFormatToBeConvertedToPiece);
                    }
                    x += 1;
                }
                x = 0;
                y += 1;
            }
            y = 0;
        }

        if ret.is_empty() {
            return Err(Error::InvalidFormatToBeConvertedToPiece);
        }

        ret.sort();

        Ok(ret.into())
    }
}

impl From<LabeledPoint> for Piece {
    fn from(lp: LabeledPoint) -> Self {
        Self {
            points: vec![lp.point().clone()],
            label: lp.label().into(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::Result;

    impl Piece {
        /// 以下の場合不正なピースとする
        /// - データがソートされていない
        /// - 同じマスにピースがある
        /// - 隣接していないマスがある
        pub fn validate(&self) -> Result<(), Error> {
            if self.points.is_empty() {
                return Err(Error::InvalidPieceIsEmpty);
            }

            for pair in self.points.windows(2) {
                if pair.first() > pair.get(1) {
                    return Err(Error::InvalidPieceIsNotSorted);
                }

                if pair.first() == pair.get(1) {
                    return Err(Error::InvalidPieceHasSamePoint(
                        pair.first().unwrap().clone(), // 上で is_empty の場合は来ない
                    ));
                }
            }

            let mut rest = self.points.clone();
            let mut bag = vec![rest.pop().unwrap()]; // 上で is_empty の場合は来ない

            while !rest.is_empty() {
                let mut rest_inner = vec![];
                let mut found_outer = false;
                for current in rest.into_iter() {
                    let mut found = false;
                    for in_bag in bag.iter() {
                        if in_bag.neighbor(&current) {
                            bag.push(current.clone());
                            found = true;
                            found_outer = true;
                            break;
                        }
                    }
                    if !found {
                        rest_inner.push(current);
                    }
                }

                if !found_outer {
                    return Err(Error::InvalidPieceIsSeperated);
                }
                rest = rest_inner;
            }

            Ok(())
        }
    }

    #[test]
    fn test_validate() -> Result<()> {
        // InvalidPieceIsEmpty
        let piece = Piece::from(vec![]);
        assert!(matches!(piece.validate(), Err(Error::InvalidPieceIsEmpty)));

        // InvalidPieceHasSamePoint
        let piece = Piece::from(vec![Point::new(0, 0, 0), Point::new(0, 0, 0)]);
        assert!(matches!(
            piece.validate(),
            Err(Error::InvalidPieceHasSamePoint(_))
        ));

        // InvalidPieceIsNotSorted
        let piece = Piece::from(vec![Point::new(1, 0, 0), Point::new(0, 0, 0)]);
        assert!(matches!(
            piece.validate(),
            Err(Error::InvalidPieceIsNotSorted)
        ));

        // InvalidPieceIsSeperated
        let piece = Piece::from(vec![Point::new(0, 0, 0), Point::new(2, 0, 0)]);
        assert!(matches!(
            piece.validate(),
            Err(Error::InvalidPieceIsSeperated)
        ));

        // Ok
        let piece = Piece::from(vec![
            Point::new(0, 0, 0),
            Point::new(1, 0, 0),
            Point::new(2, 0, 0),
        ]);
        assert!(piece.validate().is_ok());

        Ok(())
    }
}
