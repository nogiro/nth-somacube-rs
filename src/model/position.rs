use crate::model::{Piece, Point};
use std::sync::Arc;

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Position {
    pub piece: Arc<Piece>,
    pub offset: Arc<Point>,
    pub origin: Arc<Piece>,
}
