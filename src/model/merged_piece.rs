use crate::model::{AddOffset, LabeledPoint, Piece, Point, PointCollection, TakableFromSpace};
use std::collections::BTreeSet;

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct MergedPiece {
    points: BTreeSet<LabeledPoint>,
}

impl MergedPiece {
    pub fn new(piece: Piece) -> Self {
        let points: BTreeSet<LabeledPoint> = piece.into();

        Self { points }
    }

    pub fn merge(self, mp: Self) -> Self {
        let mut points = self.points;
        points.extend(mp.points);

        Self { points }
    }

    pub fn shift_near_origin(self) -> Self {
        let (min_x, min_y, min_z) = self.points.iter().fold(
            (i16::MAX, i16::MAX, i16::MAX),
            |(mut min_x, mut min_y, mut min_z), lp| {
                if lp.point().x < min_x {
                    min_x = lp.point().x;
                }

                if lp.point().y < min_y {
                    min_y = lp.point().y;
                }

                if lp.point().z < min_z {
                    min_z = lp.point().z;
                }

                (min_x, min_y, min_z)
            },
        );

        let offset = Point::new(-min_x, -min_y, -min_z);
        self.add_offset(&offset)
    }

    pub fn points(&self) -> &BTreeSet<LabeledPoint> {
        &self.points
    }

    pub fn len(&self) -> usize {
        self.points.len()
    }

    pub fn is_empty(&self) -> bool {
        self.points.is_empty()
    }

    pub fn labels(&self) -> String {
        self.points
            .iter()
            .map(|x| x.label().to_string())
            .collect::<BTreeSet<_>>()
            .into_iter()
            .collect::<Vec<_>>()
            .join("")
    }

    #[cfg(test)]
    pub fn neighbors(&self, size: i16) -> BTreeSet<Point> {
        self.points
            .iter()
            .flat_map(|lp| lp.point().neighbors(size))
            .filter(|p| self.points.iter().all(|lp| lp.point() != p))
            .collect()
    }
}

impl PointCollection for MergedPiece {
    type PointItem = LabeledPoint;
    type PointIter<'a> = std::collections::btree_set::Iter<'a, LabeledPoint>;

    fn point_iter(&self) -> Self::PointIter<'_> {
        self.points.iter()
    }
    fn map_to_point<'a>(&self, p: &'a Self::PointItem) -> &'a Point {
        p.point()
    }
}

impl TakableFromSpace for MergedPiece {
    fn labeled_point(&self) -> Vec<LabeledPoint> {
        self.points.iter().cloned().collect()
    }
}

impl AddOffset for MergedPiece {
    fn add_offset(&self, p: &Point) -> Self {
        Self {
            points: self
                .points
                .iter()
                .cloned()
                .map(|point| point.add_offset(p))
                .collect(),
        }
    }
}

impl From<BTreeSet<LabeledPoint>> for MergedPiece {
    fn from(points: BTreeSet<LabeledPoint>) -> Self {
        Self { points }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::Result;

    #[test]
    fn test_neighbors() -> Result<()> {
        let mp = MergedPiece::new(Piece::try_from("**|*-")?);
        let ns = mp.neighbors(2);

        let expect: BTreeSet<_> = Piece::try_from(
            r#"
                --|-*
                **|*-
            "#,
        )?
        .into_point_iter()
        .collect();
        assert_eq!(ns, expect);

        let mp = MergedPiece::new(Piece::try_from(
            r#"
                ---|---|---
                -*-|-*-|-*-
                ---|---|---
            "#,
        )?);
        let ns = mp.neighbors(3);

        let expect: BTreeSet<_> = Piece::try_from(
            r#"
                -*-|-*-|-*-
                *-*|*-*|*-*
                -*-|-*-|-*-
            "#,
        )?
        .into_point_iter()
        .collect();
        assert_eq!(ns, expect);

        Ok(())
    }
}
