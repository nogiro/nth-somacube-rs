use crate::model::Point;

pub trait PointCollection {
    type PointItem;
    type PointIter<'a>: std::iter::Iterator<Item = &'a Self::PointItem>
    where
        Self: 'a;

    fn point_iter(&self) -> Self::PointIter<'_>;
    fn map_to_point<'a>(&self, p: &'a Self::PointItem) -> &'a Point;
}
