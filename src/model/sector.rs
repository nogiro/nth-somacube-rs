use crate::model::{Point, Space};
use std::collections::BTreeSet;

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Sector {
    points: BTreeSet<Point>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Sectors {
    sectors: BTreeSet<Sector>,
}

impl Sector {
    pub fn neighbor(&self, rhs: &Self) -> bool {
        self.points
            .iter()
            .any(|lp| rhs.points.iter().any(|rp| lp.neighbor(rp)))
    }

    pub fn merge(self, rhs: Self) -> Self {
        Self {
            points: self.points.into_iter().chain(rhs.points).collect(),
        }
    }
}

impl Sectors {
    pub fn from_points(points: BTreeSet<Point>) -> Self {
        let mut targets: Vec<_> = points.into_iter().map(Sector::from).collect();
        let mut fin = BTreeSet::new();

        let mut merged = true;
        while merged {
            let mut it = targets.into_iter();
            let Some(mut current_target) = it.next() else {
                break;
            };

            let mut rest = vec![];
            merged = false;

            for current in it {
                if current_target.neighbor(&current) {
                    merged = true;
                    current_target = current_target.merge(current);
                } else {
                    rest.push(current);
                }
            }

            if merged {
                rest.push(current_target);
            } else {
                fin.insert(current_target);
                merged = !rest.is_empty();
            };
            targets = rest;
        }

        Self { sectors: fin }
    }

    pub fn min_len(&self) -> usize {
        self.sectors
            .iter()
            .map(|sector| sector.points.len())
            .min()
            .unwrap_or_default()
    }

    pub fn len(&self) -> usize {
        self.sectors.len()
    }

    pub fn is_empty(&self) -> bool {
        self.sectors.is_empty()
    }
}

impl From<Point> for Sector {
    fn from(p: Point) -> Self {
        Sector {
            points: BTreeSet::from([p]),
        }
    }
}

impl From<&Space> for Sectors {
    fn from(sp: &Space) -> Self {
        Sectors::from_points(sp.iter().cloned().collect())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::model::Piece;
    use anyhow::Result;

    #[test]
    fn test_sectors() -> Result<()> {
        let space = Space::new(3);

        println!("1");
        let sectors = Sectors::from(&space);
        assert_eq!(sectors.sectors.len(), 1);

        println!("2");
        let piece = Piece::try_from(
            r#"
                ---|***|---
                ---|***|---
                ---|***|---
            "#,
        )?;
        let space = space.take(&piece).unwrap();

        let sectors = Sectors::from(&space);
        assert_eq!(sectors.sectors.len(), 2);

        println!("3");
        let piece = Piece::try_from(
            r#"
                ---|---|---
                ***|---|---
                ---|---|---
            "#,
        )?;
        let space = space.take(&piece).unwrap();

        let sectors = Sectors::from(&space);
        assert_eq!(sectors.sectors.len(), 3);

        println!("4");
        let piece = Piece::try_from(
            r#"
                ---|---|---
                ---|---|***
                ---|---|---
            "#,
        )?;
        let space = space.take(&piece).unwrap();

        let sectors = Sectors::from(&space);
        assert_eq!(sectors.sectors.len(), 4);

        Ok(())
    }
}
