use crate::model::{LabeledPoint, Point, PointCollection};
use std::collections::BTreeSet;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Space {
    points: BTreeSet<Point>,
    size: i16,
    placed: BTreeSet<LabeledPoint>,
}

pub trait TakableFromSpace: PointCollection {
    fn labeled_point(&self) -> Vec<LabeledPoint>;
}

impl Space {
    pub fn new(size: i16) -> Self {
        let mut points = BTreeSet::new();
        for x in 0..size {
            for y in 0..size {
                for z in 0..size {
                    points.insert(Point::new(x, y, z));
                }
            }
        }
        Self {
            points,
            size,
            placed: Default::default(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.points.is_empty()
    }

    pub fn takable(&self, points: &impl TakableFromSpace) -> bool {
        let mut point_iter = points.point_iter().map(|p| points.map_to_point(p));

        if self.points.len() < self.placed.len() {
            point_iter.all(|p| self.points.contains(p))
        } else {
            point_iter.all(|p| {
                if p.is_outer(self.size) {
                    false
                } else {
                    self.placed.iter().all(|p2| p2.point() != p)
                }
            })
        }
    }

    pub fn take(&self, ps: &impl TakableFromSpace) -> Option<Self> {
        if !self.takable(ps) {
            return None;
        }

        let mut points = self.points.clone();
        for p in ps.point_iter().map(|p| ps.map_to_point(p)) {
            points.remove(p);
        }

        let mut placed = self.placed.clone();
        placed.extend(ps.labeled_point());

        Some(Self {
            points,
            size: self.size,
            placed,
        })
    }

    pub fn iter(&self) -> std::collections::btree_set::Iter<Point> {
        self.points.iter()
    }

    pub fn len(&self) -> usize {
        self.points.len()
    }

    pub fn has(&self, point: &Point) -> bool {
        self.points.contains(point)
    }

    pub fn size(&self) -> i16 {
        self.size
    }

    pub fn has_single_cell(&self) -> bool {
        if self.points.len() == 1 {
            return true;
        }

        self.points.iter().any(|p1| {
            !p1.neighbors(self.size)
                .into_iter()
                .any(|p2| self.points.contains(&p2))
        })
    }

    pub fn neighbors(&self, point: &Point) -> Vec<Point> {
        point
            .neighbors(self.size)
            .into_iter()
            .filter(|p1| self.points.contains(p1))
            .collect()
    }

    pub fn neighbors_count_variance(&self) -> i32 {
        let sum: i32 = self
            .points
            .iter()
            .map(|p1| self.neighbors(p1).len() as i32)
            .sum();

        self.points
            .iter()
            .map(|p1| ((self.neighbors(p1).len() * self.len()) as i32 - sum).abs())
            .sum()
    }

    pub fn placed(&self) -> &BTreeSet<LabeledPoint> {
        &self.placed
    }

    pub fn hash_value(&self) -> u64 {
        use std::hash::{DefaultHasher, Hash, Hasher};

        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        hasher.finish()
    }

    pub fn first(&self) -> Option<&Point> {
        self.points.first()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::model::{AddOffset, Piece};
    use anyhow::Result;

    #[test]
    fn test_takable() -> Result<()> {
        let space = Space::new(1);
        let piece = Piece::try_from("*")?;
        assert!(space.takable(&piece));

        assert!(!space.takable(&piece.add_offset(&Point::new(-1, 0, 0))));
        assert!(!space.takable(&piece.add_offset(&Point::new(1, 0, 0))));
        assert!(!space.takable(&piece.add_offset(&Point::new(0, -1, 0))));
        assert!(!space.takable(&piece.add_offset(&Point::new(0, 1, 0))));
        assert!(!space.takable(&piece.add_offset(&Point::new(0, 0, -1))));
        assert!(!space.takable(&piece.add_offset(&Point::new(0, 0, 1))));

        Ok(())
    }

    #[test]
    fn test_has_single_cell() -> Result<()> {
        let space = Space::new(1);
        assert!(space.has_single_cell());

        let space = Space::new(2);
        assert!(!space.has_single_cell());

        let piece = Piece::try_from(
            r#"
                -*|*-
                *-|*-
            "#,
        )?;
        let space = Space::new(2).take(&piece).unwrap();
        assert!(space.has_single_cell());

        let piece = Piece::try_from(
            r#"
                --*|**-|***
                **-|**-|***
                ***|***|***
            "#,
        )?;
        let space = Space::new(3).take(&piece).unwrap();
        assert!(!space.has_single_cell());

        Ok(())
    }
}
