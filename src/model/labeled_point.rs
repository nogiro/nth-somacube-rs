use crate::model::{
    AddOffset, Piece, PieceLabel, Point, PointCollection, Rotate, TakableFromSpace,
};
use std::collections::BTreeSet;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct LabeledPoint {
    point: Point,
    label: PieceLabel,
}

impl LabeledPoint {
    pub fn point(&self) -> &Point {
        &self.point
    }

    pub fn label(&self) -> &'static char {
        self.label.0
    }
}

impl AddOffset for LabeledPoint {
    fn add_offset(&self, point: &Point) -> Self {
        Self {
            point: self.point.add_offset(point),
            label: self.label,
        }
    }
}

impl PointCollection for Vec<LabeledPoint> {
    type PointItem = LabeledPoint;
    type PointIter<'a> = std::slice::Iter<'a, LabeledPoint>;

    fn point_iter(&self) -> Self::PointIter<'_> {
        self.iter()
    }
    fn map_to_point<'a>(&self, p: &'a Self::PointItem) -> &'a Point {
        &p.point
    }
}

impl TakableFromSpace for Vec<LabeledPoint> {
    fn labeled_point(&self) -> Vec<LabeledPoint> {
        self.clone()
    }
}

impl PointCollection for BTreeSet<LabeledPoint> {
    type PointItem = LabeledPoint;
    type PointIter<'a> = std::collections::btree_set::Iter<'a, LabeledPoint>;

    fn point_iter(&self) -> Self::PointIter<'_> {
        self.iter()
    }
    fn map_to_point<'a>(&self, p: &'a Self::PointItem) -> &'a Point {
        &p.point
    }
}

impl TakableFromSpace for BTreeSet<LabeledPoint> {
    fn labeled_point(&self) -> Vec<LabeledPoint> {
        self.iter().cloned().collect()
    }
}

impl Rotate for Vec<LabeledPoint> {
    /// x 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_x(&self, num: u16) -> Self {
        self.iter()
            .map(|lp| (lp.point.rotate_x(num), lp.label.0).into())
            .collect()
    }

    /// y 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_y(&self, num: u16) -> Self {
        self.iter()
            .map(|lp| (lp.point.rotate_y(num), lp.label.0).into())
            .collect()
    }

    /// z 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_z(&self, num: u16) -> Self {
        self.iter()
            .map(|lp| (lp.point.rotate_z(num), lp.label.0).into())
            .collect()
    }
}

impl Rotate for BTreeSet<LabeledPoint> {
    /// x 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_x(&self, num: u16) -> Self {
        self.iter()
            .map(|lp| (lp.point.rotate_x(num), lp.label.0).into())
            .collect()
    }

    /// y 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_y(&self, num: u16) -> Self {
        self.iter()
            .map(|lp| (lp.point.rotate_y(num), lp.label.0).into())
            .collect()
    }

    /// z 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_z(&self, num: u16) -> Self {
        self.iter()
            .map(|lp| (lp.point.rotate_z(num), lp.label.0).into())
            .collect()
    }
}

impl PartialOrd for LabeledPoint {
    fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering> {
        Some(self.point.cmp(&rhs.point))
    }
}

impl Ord for LabeledPoint {
    fn cmp(&self, rhs: &Self) -> std::cmp::Ordering {
        self.point.cmp(&rhs.point)
    }
}

impl From<Piece> for Vec<LabeledPoint> {
    fn from(piece: Piece) -> Self {
        let label = piece.label().into();
        piece
            .into_point_iter()
            .map(|point| LabeledPoint { point, label })
            .collect()
    }
}

impl From<Piece> for BTreeSet<LabeledPoint> {
    fn from(piece: Piece) -> Self {
        let label = piece.label().into();
        piece
            .into_point_iter()
            .map(|point| LabeledPoint { point, label })
            .collect()
    }
}

impl From<(Point, &'static char)> for LabeledPoint {
    fn from((point, label): (Point, &'static char)) -> Self {
        Self {
            point,
            label: label.into(),
        }
    }
}
