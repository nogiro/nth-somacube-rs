use crate::model::Rotate;

/// ```
/// # use nth_somacube_rs as sc;
/// # use sc::model::Point;
/// let p1 = Point::new(0,0,0);
/// let p2 = Point::new(1,0,0);
/// let p3 = Point::new(1,1,0);
/// let p4 = Point::new(1,1,1);
/// assert!(p1 < p2);
/// assert!(p1 < p3);
/// assert!(p1 < p4);
/// assert!(p2 < p3);
/// assert!(p2 < p4);
/// assert!(p3 < p4);
/// ```
#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Point {
    pub x: i16,
    pub y: i16,
    pub z: i16,
}

pub trait AddOffset {
    fn add_offset(&self, rhs: &Point) -> Self;
}

impl Point {
    pub fn new(x: i16, y: i16, z: i16) -> Self {
        Self { x, y, z }
    }

    /// [`Point`] 同士が隣り合っているかを判定する
    ///
    /// ```
    /// # use nth_somacube_rs as sc;
    /// # use sc::model::Point;
    /// let p1 = Point::new(0,0,0);
    /// let p2 = Point::new(1,0,0);
    /// assert!(p1.neighbor(&p2));
    /// ```
    pub fn neighbor(&self, rhs: &Self) -> bool {
        let x_abs = self.x.abs_diff(rhs.x);
        let y_abs = self.y.abs_diff(rhs.y);
        let z_abs = self.z.abs_diff(rhs.z);

        x_abs + y_abs + z_abs == 1
    }

    pub fn neighbors(&self, size: i16) -> Vec<Self> {
        [
            Point::new(self.x - 1, self.y, self.z),
            Point::new(self.x + 1, self.y, self.z),
            Point::new(self.x, self.y - 1, self.z),
            Point::new(self.x, self.y + 1, self.z),
            Point::new(self.x, self.y, self.z - 1),
            Point::new(self.x, self.y, self.z + 1),
        ]
        .into_iter()
        .filter(|p| 0 <= p.x && p.x < size && 0 <= p.y && p.y < size && 0 <= p.z && p.z < size)
        .collect()
    }

    /// 2 点間の差を計算する
    pub fn difference(&self, rhs: &Self) -> Self {
        Self::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }

    pub fn is_outer(&self, size: i16) -> bool {
        self.x < 0 || size <= self.x || self.y < 0 || size <= self.y || self.z < 0 || size <= self.z
    }
}

impl AddOffset for Point {
    /// 移動する
    fn add_offset(&self, rhs: &Self) -> Self {
        Self::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl Rotate for Point {
    /// x 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_x(&self, num: u16) -> Self {
        match num % 4 {
            0 => self.clone(),
            1 => Self::new(self.x, self.z, -self.y),
            2 => Self::new(self.x, -self.y, -self.z),
            3 => Self::new(self.x, -self.z, self.y),
            _ => unreachable!(),
        }
    }

    /// y 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_y(&self, num: u16) -> Self {
        match num % 4 {
            0 => self.clone(),
            1 => Self::new(-self.z, self.y, self.x),
            2 => Self::new(-self.x, self.y, -self.z),
            3 => Self::new(self.z, self.y, -self.x),
            _ => unreachable!(),
        }
    }

    /// z 軸に沿って回転する。左手系・右手系はあんまり気にしてない。
    fn rotate_z(&self, num: u16) -> Self {
        match num % 4 {
            0 => self.clone(),
            1 => Self::new(self.y, -self.x, self.z),
            2 => Self::new(-self.x, -self.y, self.z),
            3 => Self::new(-self.y, self.x, self.z),
            _ => unreachable!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::Result;

    #[test]
    fn test_rotate() -> Result<()> {
        let point = Point::new(2, 1, 0);
        assert_eq!(point.rotate_x(0), Point::new(2, 1, 0));
        assert_eq!(point.rotate_x(1), Point::new(2, 0, -1));
        assert_eq!(point.rotate_x(2), Point::new(2, -1, 0));
        assert_eq!(point.rotate_x(3), Point::new(2, 0, 1));

        let point = Point::new(0, 2, 1);
        assert_eq!(point.rotate_y(0), Point::new(0, 2, 1));
        assert_eq!(point.rotate_y(1), Point::new(-1, 2, 0));
        assert_eq!(point.rotate_y(2), Point::new(0, 2, -1));
        assert_eq!(point.rotate_y(3), Point::new(1, 2, 0));

        let point = Point::new(1, 0, 2);
        assert_eq!(point.rotate_z(0), Point::new(1, 0, 2));
        assert_eq!(point.rotate_z(1), Point::new(0, -1, 2));
        assert_eq!(point.rotate_z(2), Point::new(-1, 0, 2));
        assert_eq!(point.rotate_z(3), Point::new(0, 1, 2));

        Ok(())
    }
}
