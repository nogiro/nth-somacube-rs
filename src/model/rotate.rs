use std::collections::BTreeSet;

pub trait Rotate {
    fn rotate_x(&self, num: u16) -> Self;
    fn rotate_y(&self, num: u16) -> Self;
    fn rotate_z(&self, num: u16) -> Self;

    /// 回転した図形一覧を得る (「6 面のどれを上にするか」x「4 方位」の 24 通りを計算してから重複を消す)
    fn rotate_set(&self) -> Vec<Self>
    where
        Self: Sized + Ord,
    {
        // 6 面
        let planes = vec![
            self.rotate_x(0),
            self.rotate_x(1),
            self.rotate_x(2),
            self.rotate_x(3),
            self.rotate_z(1),
            self.rotate_z(3),
        ];

        let ret: BTreeSet<_> = planes
            .into_iter()
            .flat_map(|x| {
                // 4 方位
                vec![x.rotate_y(0), x.rotate_y(1), x.rotate_y(2), x.rotate_y(3)]
            })
            // BTreeSet へ collect して重複を消す
            .collect();

        ret.into_iter().collect()
    }
}
