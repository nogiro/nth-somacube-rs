use anyhow::{Context as _, Result};
use tracing::subscriber::set_global_default;
use tracing_subscriber::EnvFilter;

const ENV_NAME: &str = "SOMACUBE_LOG";

pub fn init_logger(level: u8) -> Result<()> {
    let env_filter = env_filter(level);

    let subscriber = tracing_subscriber::fmt()
        .compact()
        .with_writer(std::io::stderr)
        .with_file(true)
        .with_line_number(true)
        .with_target(true)
        .with_env_filter(env_filter)
        .finish();

    set_global_default(subscriber).context("failed to set tracing subscriber to global")?;
    Ok(())
}

fn env_filter(level: u8) -> EnvFilter {
    let log_level = std::env::var(ENV_NAME).unwrap_or_else(|_| {
        match level {
            0 => "warn",
            1 => "info",
            2 => "debug",
            _ => "trace",
        }
        .to_string()
    });
    EnvFilter::new(log_level)
}
