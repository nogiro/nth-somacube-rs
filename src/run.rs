use crate::config::Algorithm;
use crate::model::{Piece, Space};
use crate::solver::{
    LeastVacantNeighborsCandidates, LvnAndLowVarianceInSpaceCandidates, LvnalvisNoRandomCandidates,
    MergedPieceState, NoRandomCandidates, RandomCandidates,
};
use crate::Config;
use crate::{Args, ChoiceOneState, Error, SizeMode, Solver};

pub fn run(args: Args) -> Result<(), Error> {
    let (space, piece) = match args.size {
        SizeMode::Third => (Space::new(3), Piece::seven_soma_pieces()),
        SizeMode::Forth => (Space::new(4), Piece::double_tetracube_pieces()),
        SizeMode::Fifth => (Space::new(5), Piece::pentacube_pieces()),
    };

    let config: Config = (&args).into();

    let algorithm = config.algorithm.clone();

    let mut solver = Solver::new(config);

    match algorithm {
        Algorithm::MergePiece => {
            let init = MergedPieceState::new(space, piece);
            solver.solve(args.count, init)?;
        }

        Algorithm::LvnalvisNoRandom => {
            let init = ChoiceOneState::<LvnalvisNoRandomCandidates>::new(space, piece);
            solver.solve(args.count, init)?;
        }

        Algorithm::LvnAndLowVarianceInSpace => {
            let init = ChoiceOneState::<LvnAndLowVarianceInSpaceCandidates>::new(space, piece);
            solver.solve(args.count, init)?;
        }

        Algorithm::LeastVacantNeighbors => {
            let init = ChoiceOneState::<LeastVacantNeighborsCandidates>::new(space, piece);
            solver.solve(args.count, init)?;
        }

        Algorithm::Random => {
            let init = ChoiceOneState::<RandomCandidates>::new(space, piece);
            solver.solve(args.count, init)?;
        }

        Algorithm::NoRandom => {
            let init = ChoiceOneState::<NoRandomCandidates>::new(space, piece);
            solver.solve(args.count, init)?;
        }
    }

    Ok(())
}
