use crate::{Args, SizeMode};
use std::num::NonZeroUsize;

#[derive(Debug)]
pub struct Config {
    pub seed: Option<u64>,
    pub pruning_secter_splitted_upper_limit: usize,
    pub pruning_secter_upper_limit: usize,
    pub algorithm: Algorithm,
    pub cache_size: NonZeroUsize,
    pub in_order_from_the_edge: bool,
}

impl From<&Args> for Config {
    fn from(args: &Args) -> Self {
        let pruning_secter_splitted_upper_limit = args
            .pruning_secter_splitted_upper_limit
            .unwrap_or_else(|| default_pruning_secter_splitted_upper_limit(&args.size));

        let pruning_secter_upper_limit = args
            .pruning_secter_upper_limit
            .unwrap_or_else(|| default_pruning_secter_upper_limit(&args.size));

        Self {
            seed: args.seed,
            pruning_secter_splitted_upper_limit,
            pruning_secter_upper_limit,
            algorithm: args.algorithm.clone().into(),
            cache_size: args.cache_size,
            in_order_from_the_edge: !args.not_in_order_from_the_edge,
        }
    }
}

#[derive(Debug, Clone)]
pub enum Algorithm {
    MergePiece,
    LvnalvisNoRandom,
    LvnAndLowVarianceInSpace,
    LeastVacantNeighbors,
    Random,
    NoRandom,
}

fn default_pruning_secter_splitted_upper_limit(size: &SizeMode) -> usize {
    match size {
        SizeMode::Third => 10,
        SizeMode::Forth => 20,
        SizeMode::Fifth => 40,
    }
}

fn default_pruning_secter_upper_limit(size: &SizeMode) -> usize {
    match size {
        SizeMode::Third => 16,
        SizeMode::Forth => 29,
        SizeMode::Fifth => 50,
    }
}
