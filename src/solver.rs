mod candidates;
mod state;
mod strategy;

pub use crate::solver::candidates::{
    Candidates, LeastVacantNeighborsCandidates, LvnAndLowVarianceInSpaceCandidates,
    LvnalvisNoRandomCandidates, MemoToEnumerateCandidates, NoRandomCandidates, RandomCandidates,
};
pub use crate::solver::state::{ChoiceOneState, MergedPieceState};
pub use crate::solver::strategy::Strategy;

use crate::solver::state::{SolvedState, State};
use crate::{Config, Error};
use hashbrown::HashMap;
use rand::rngs::StdRng;
use rand::SeedableRng;

pub struct Solver {
    rng: StdRng,
    strategy: Strategy,
    memo: MemoToEnumerateCandidates,
}

impl Solver {
    pub fn new(config: Config) -> Self {
        tracing::info!("config = {config:?}");

        let rng = match config.seed {
            Some(seed) => StdRng::seed_from_u64(seed),
            None => StdRng::from_entropy(),
        };

        let memo = MemoToEnumerateCandidates::new(config.cache_size);
        let strategy = config.into();

        Self {
            rng,
            strategy,
            memo,
        }
    }

    pub fn solve<S>(&mut self, count: usize, mut state: S) -> Result<(), Error>
    where
        S: State,
    {
        state.init_memo(&mut self.memo);

        state.fill_candidates(self);

        tracing::info!("start: count = {count}");
        state.starting_log();
        let piece_len = state.rest_len();
        let mut backtrace = vec![state];
        backtrace.reserve(piece_len);
        let mut solved = HashMap::new();

        while solved.len() < count {
            tracing::debug!("trace len: {}", backtrace.len());

            let current_state = match backtrace.pop() {
                Some(x) => x,
                None => break,
            };

            match current_state.step(self)? {
                Some((current_state, None)) => {
                    current_state.remained_log();
                    backtrace.push(current_state);
                }

                Some((current_state, Some(new_state))) => {
                    current_state.advanced_log(&new_state);

                    backtrace.push(current_state);

                    if let Some(solved_state) = new_state.solved() {
                        solved.insert(solved_state.hash(), solved_state);
                        continue;
                    }

                    backtrace.push(new_state);
                }

                None => {
                    S::fallback_log(backtrace.last());
                }
            }
        }

        let ret = if backtrace.is_empty() {
            tracing::info!("exhausted!");
            Err(Error::BackTraceIsExhausted)
        } else {
            tracing::info!("solved!");
            Ok(())
        };

        for solved in solved.into_values() {
            solved.display();
        }

        ret
    }

    pub fn strategy(&self) -> &Strategy {
        &self.strategy
    }
}
