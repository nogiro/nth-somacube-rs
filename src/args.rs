mod algorithm;
mod size_mode;

pub use crate::args::algorithm::Algorithm;
pub use crate::args::size_mode::SizeMode;

use clap::Parser;
use std::num::NonZeroUsize;

#[derive(Parser, Debug)]
#[command(version, about = None, long_about = None)]
pub struct Args {
    /// 計算対象。
    #[arg(default_value_t = SizeMode::Third)]
    pub size: SizeMode,

    /// ここで指定した数の回答が揃うまで計算を続ける。
    #[arg(short, long, default_value_t = 3)]
    pub count: usize,

    /// increase log level: `-v` -> info, `-vv` -> debug.
    #[arg(short, long, action = clap::ArgAction::Count)]
    pub verbose: u8,

    /// 結果に再現性を与えるための擬似乱数生成器へのシード。
    #[arg(short, long)]
    pub seed: Option<u64>,

    /// 残りの空きマスを塊ごとに分けて、塊が複数に分割されていたときに枝刈りを行う処理を、残りの空きマス数がいくつ以下で行うかを指定する。
    #[arg(long)]
    pub pruning_secter_splitted_upper_limit: Option<usize>,

    /// 残りの空きマスを塊ごとに分けて、その中で一番小さい塊の大きさが、残りのピースの最小の大きさより小さい場合に枝刈りを行う際の、残りの空きマス数がいくつ以下で行うかを指定する。
    #[arg(long)]
    pub pruning_secter_upper_limit: Option<usize>,

    #[arg(long, default_value_t = Algorithm::LvnAndLowVarianceInSpace)]
    pub algorithm: Algorithm,

    #[arg(long, default_value = "65536")]
    pub cache_size: NonZeroUsize,

    /// ChoiceOne するときに、端から順番に空きマスを選択する
    #[arg(long)]
    pub not_in_order_from_the_edge: bool,
}

#[cfg(test)]
mod tests {
    use super::*;

    use clap::CommandFactory;

    #[test]
    fn test_command() {
        Args::command().debug_assert()
    }
}
