use nth_somacube_rs as sc;

use anyhow::{Context as _, Result};
use clap::Parser;
use sc::Args;

fn main() -> Result<(), sc::Error> {
    let args = Args::parse();

    sc::init_logger(args.verbose).context("init_logger(): failed")?;
    tracing::info!("args = {args:?}");

    sc::run(args)
}
