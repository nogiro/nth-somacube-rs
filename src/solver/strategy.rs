use crate::Config;

pub struct Strategy {
    pruning_secter_splitted_upper_limit: usize,
    pruning_secter_upper_limit: usize,
    in_order_from_the_edge: bool,
}

impl Strategy {
    /// 空きマスセクターが分割されているかによって枝刈りを行うかを判定する
    pub fn do_prune_by_secter_splitted(&self, space_len: usize) -> bool {
        space_len < self.pruning_secter_splitted_upper_limit
    }

    /// セクターの大きさに応じて孤立セクターのサイズと残りピースのサイズ比較による枝刈りを行うかを判定する
    pub fn do_prune_by_secter_size(&self, space_len: usize) -> bool {
        space_len < self.pruning_secter_upper_limit
    }

    /// ChoiceOne するときに、端から順番に空きマスを選択する
    pub fn in_order_from_the_edge(&self) -> bool {
        self.in_order_from_the_edge
    }
}

impl From<Config> for Strategy {
    fn from(config: Config) -> Self {
        Self {
            pruning_secter_splitted_upper_limit: config.pruning_secter_splitted_upper_limit,
            pruning_secter_upper_limit: config.pruning_secter_upper_limit,
            in_order_from_the_edge: config.in_order_from_the_edge,
        }
    }
}
