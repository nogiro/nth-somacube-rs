mod least_vacant_neighbors;
mod lvn_and_low_variance_in_space;
mod lvnalvis_no_random_candidates;
mod no_random;
mod random;

pub use least_vacant_neighbors::LeastVacantNeighborsCandidates;
pub use lvn_and_low_variance_in_space::LvnAndLowVarianceInSpaceCandidates;
pub use lvnalvis_no_random_candidates::LvnalvisNoRandomCandidates;
pub use no_random::NoRandomCandidates;
pub use random::RandomCandidates;

use crate::model::{Piece, PieceMetadata, Position, Space};
use crate::Solver;
use hashbrown::HashMap;
use lru::LruCache;
use std::num::NonZeroUsize;

pub trait Candidates:
    std::iter::IntoIterator<Item = Position, IntoIter = <Vec<Position> as IntoIterator>::IntoIter>
    + From<Vec<Position>>
    + std::fmt::Debug
{
    fn none() -> Self;

    fn len(&self) -> usize;
    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    fn new(space: &Space, pieces: &HashMap<Piece, PieceMetadata>, solver: &mut Solver) -> Self;
    fn iter(&self) -> std::slice::Iter<'_, Position>;
}

pub struct MemoToEnumerateCandidates {
    variance: Vec<LruCache<Space, i32>>,
    cache_size: NonZeroUsize,
}

impl MemoToEnumerateCandidates {
    pub fn new(cache_size: NonZeroUsize) -> Self {
        Self {
            variance: Default::default(),
            cache_size,
        }
    }

    pub fn init(&mut self, size: i16) {
        for _ in 0..size {
            self.variance.push(LruCache::new(self.cache_size));
        }
    }
}
