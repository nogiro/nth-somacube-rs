mod choice_one;
mod merge_piece;

pub use crate::solver::state::choice_one::ChoiceOneState;
pub use crate::solver::state::merge_piece::MergedPieceState;

use crate::model::LabeledPoint;
use crate::solver::MemoToEnumerateCandidates;
use crate::Error;
use crate::Solver;
use std::collections::BTreeSet;

pub trait State {
    type Solved: SolvedState;

    fn solved(&self) -> Option<Self::Solved>;

    fn rest_len(&self) -> usize;

    fn init_memo(&self, memo: &mut MemoToEnumerateCandidates);

    fn rest_candidates_len(&self) -> usize;

    fn fill_candidates(&mut self, solver: &mut Solver);

    fn step(self, solver: &mut Solver) -> Result<Option<(Self, Option<Self>)>, Error>
    where
        Self: Sized;

    fn starting_log(&self);
    fn remained_log(&self);
    fn advanced_log(&self, next: &Self);
    fn fallback_log(prev: Option<&Self>);
}

pub trait SolvedState {
    fn hash(&self) -> BTreeSet<LabeledPoint>;
    fn display(&self);
}
