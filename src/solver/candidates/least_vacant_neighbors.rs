use crate::model::{Piece, PieceMetadata, Position, Space};
use crate::solver::candidates::Candidates;
use crate::Solver;
use hashbrown::HashMap;
use rand::seq::SliceRandom;
use std::collections::BTreeSet;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct LeastVacantNeighborsCandidates {
    candidates: Vec<Position>,
}

impl Candidates for LeastVacantNeighborsCandidates {
    fn len(&self) -> usize {
        self.candidates.len()
    }

    fn none() -> Self {
        Self { candidates: vec![] }
    }

    fn new(space: &Space, pieces: &HashMap<Piece, PieceMetadata>, solver: &mut Solver) -> Self {
        let in_order_from_the_edge = solver.strategy().in_order_from_the_edge();
        let mut buckets: Vec<_> = vec![BTreeSet::<Position>::new(); 6];

        for meta in pieces.values() {
            for ep in meta.enabled_places.iter() {
                let count = space.neighbors(&ep.offset).len();
                if count == 0 {
                    continue;
                }

                // cube の端から詰めていくために、端じゃない候補は落とす
                if in_order_from_the_edge {
                    if let Some(point_in_space) = space.first() {
                        if !ep.place.has(point_in_space) {
                            continue;
                        }
                    }
                }

                // cube 外にはめようとしてる場合は候補から外す
                if !space.takable(ep.place.as_ref()) {
                    continue;
                }

                let position = (meta, ep).into();
                let candidates = buckets.get_mut(count - 1).unwrap(); // 隣接マス数は 6 なので上で 6 個用意している
                candidates.insert(position);
            }
        }

        let mut candidates = vec![];
        for c in buckets.into_iter() {
            let mut c: Vec<_> = c.into_iter().collect();
            c.shuffle(&mut solver.rng);
            candidates.extend(c);
        }

        candidates.into()
    }

    fn iter(&self) -> std::slice::Iter<'_, Position> {
        self.candidates.iter()
    }
}

impl std::iter::IntoIterator for LeastVacantNeighborsCandidates {
    type Item = Position;
    type IntoIter = <Vec<Position> as IntoIterator>::IntoIter;

    fn into_iter(self) -> <Self as IntoIterator>::IntoIter {
        self.candidates.into_iter()
    }
}

impl From<Vec<Position>> for LeastVacantNeighborsCandidates {
    fn from(candidates: Vec<Position>) -> Self {
        Self { candidates }
    }
}
