use crate::model::{Piece, PieceMetadata, Position, Space};
use crate::solver::candidates::Candidates;
use crate::Solver;
use hashbrown::HashMap;
use std::collections::BTreeSet;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct LvnalvisNoRandomCandidates {
    candidates: Vec<Position>,
}

impl Candidates for LvnalvisNoRandomCandidates {
    fn len(&self) -> usize {
        self.candidates.len()
    }

    fn none() -> Self {
        Self { candidates: vec![] }
    }

    fn new(space: &Space, pieces: &HashMap<Piece, PieceMetadata>, solver: &mut Solver) -> Self {
        let in_order_from_the_edge = solver.strategy().in_order_from_the_edge();
        let mut buckets: Vec<_> = vec![BTreeSet::<(Position, i32)>::new(); 6];

        for meta in pieces.values() {
            for ep in meta.enabled_places.iter() {
                let count = space.neighbors(&ep.offset).len();
                if count == 0 {
                    continue;
                }

                // cube の端から詰めていくために、端じゃない候補は落とす
                if in_order_from_the_edge {
                    if let Some(point_in_space) = space.first() {
                        if !ep.place.has(point_in_space) {
                            continue;
                        }
                    }
                }

                let space = match space.take(ep.place.as_ref()) {
                    Some(sp) => sp,
                    // cube 外にはめようとしてる場合は候補から外す
                    None => continue,
                };

                // 分散をメモ化
                let variance = match solver.memo.variance.get_mut(space.len()) {
                    Some(cache) => match cache.get(&space) {
                        Some(v) => *v,
                        None => {
                            let v = space.neighbors_count_variance();
                            cache.put(space, v);
                            v
                        }
                    },
                    // space.len() ごとの memo が用意されていない場合
                    None => space.neighbors_count_variance(),
                };

                let position = (meta, ep).into();
                let candidates = buckets.get_mut(count - 1).unwrap(); // 隣接マス数は 6 なので上で 6 個用意している
                candidates.insert((position, variance));
            }
        }

        let mut candidates = vec![];
        for c in buckets.into_iter() {
            let mut c: Vec<_> = c.into_iter().collect();
            c.sort_by(|(_, lhs), (_, rhs)| lhs.cmp(rhs));
            let c: Vec<_> = c.into_iter().map(|(x, _)| x).collect();
            candidates.extend(c);
        }

        candidates.into()
    }

    fn iter(&self) -> std::slice::Iter<'_, Position> {
        self.candidates.iter()
    }
}

impl std::iter::IntoIterator for LvnalvisNoRandomCandidates {
    type Item = Position;
    type IntoIter = <Vec<Position> as IntoIterator>::IntoIter;

    fn into_iter(self) -> <Self as IntoIterator>::IntoIter {
        self.candidates.into_iter()
    }
}

impl From<Vec<Position>> for LvnalvisNoRandomCandidates {
    fn from(candidates: Vec<Position>) -> Self {
        Self { candidates }
    }
}
