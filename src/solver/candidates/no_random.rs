use crate::model::{Piece, PieceMetadata, Position, Space};
use crate::solver::candidates::Candidates;
use crate::Solver;
use hashbrown::HashMap;
use std::collections::BTreeSet;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct NoRandomCandidates {
    candidates: Vec<Position>,
}

impl NoRandomCandidates {
    pub fn into_inner(self) -> Vec<Position> {
        self.candidates
    }
}

impl Candidates for NoRandomCandidates {
    fn len(&self) -> usize {
        self.candidates.len()
    }

    fn none() -> Self {
        Self { candidates: vec![] }
    }

    fn new(space: &Space, pieces: &HashMap<Piece, PieceMetadata>, solver: &mut Solver) -> Self {
        let in_order_from_the_edge = solver.strategy().in_order_from_the_edge();
        let mut candidates = BTreeSet::new();

        for meta in pieces.values() {
            for ep in meta.enabled_places.iter() {
                // cube の端から詰めていくために、端じゃない候補は落とす
                if in_order_from_the_edge {
                    if let Some(point_in_space) = space.first() {
                        if !ep.place.has(point_in_space) {
                            continue;
                        }
                    }
                }

                // cube 外にはめようとしてる場合は候補から外す
                if !space.takable(ep.place.as_ref()) {
                    tracing::trace!("skip: space = {space:?}, place = {:?}", ep.place);
                    continue;
                }
                tracing::trace!("candidates: space = {space:?}, place = {:?}", ep.place);

                let position = (meta, ep).into();
                candidates.insert(position);
            }
        }

        candidates.into_iter().collect::<Vec<_>>().into()
    }

    fn iter(&self) -> std::slice::Iter<'_, Position> {
        self.candidates.iter()
    }
}

impl std::iter::IntoIterator for NoRandomCandidates {
    type Item = Position;
    type IntoIter = <Vec<Position> as IntoIterator>::IntoIter;

    fn into_iter(self) -> <Self as IntoIterator>::IntoIter {
        self.candidates.into_iter()
    }
}

impl From<Vec<Position>> for NoRandomCandidates {
    fn from(candidates: Vec<Position>) -> Self {
        Self { candidates }
    }
}
