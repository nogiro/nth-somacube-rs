use crate::model::{Piece, PieceMetadata, Position, Space};
use crate::solver::candidates::no_random::NoRandomCandidates;
use crate::solver::candidates::Candidates;
use crate::Solver;
use hashbrown::HashMap;
use rand::seq::SliceRandom;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct RandomCandidates {
    candidates: Vec<Position>,
}

impl Candidates for RandomCandidates {
    fn len(&self) -> usize {
        self.candidates.len()
    }

    fn none() -> Self {
        Self { candidates: vec![] }
    }

    fn new(space: &Space, pieces: &HashMap<Piece, PieceMetadata>, solver: &mut Solver) -> Self {
        let mut candidates = NoRandomCandidates::new(space, pieces, solver).into_inner();

        candidates.shuffle(&mut solver.rng);
        candidates.into()
    }

    fn iter(&self) -> std::slice::Iter<'_, Position> {
        self.candidates.iter()
    }
}

impl std::iter::IntoIterator for RandomCandidates {
    type Item = Position;
    type IntoIter = <Vec<Position> as IntoIterator>::IntoIter;

    fn into_iter(self) -> <Self as IntoIterator>::IntoIter {
        self.candidates.into_iter()
    }
}

impl From<Vec<Position>> for RandomCandidates {
    fn from(candidates: Vec<Position>) -> Self {
        Self { candidates }
    }
}
