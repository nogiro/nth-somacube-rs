use crate::model::{LabeledPoint, Piece, PieceMetadata, Sectors, Space};
use crate::solver::state::{SolvedState, State};
use crate::solver::Candidates;
use crate::solver::MemoToEnumerateCandidates;
use crate::{Error, Strategy};
use crate::{Solver, StateDisplay};
use hashbrown::HashMap;
use std::collections::BTreeSet;

pub struct Solved {
    space: Space,
    rest_pieces_label: Vec<&'static char>,
}

#[derive(Debug)]
pub struct ChoiceOneState<C>
where
    C: Candidates,
{
    space: Space,
    pieces: HashMap<Piece, PieceMetadata>,
    candidates: C,
}

impl<C> State for ChoiceOneState<C>
where
    C: Candidates,
{
    type Solved = Solved;

    fn solved(&self) -> Option<Self::Solved> {
        self.space
            .is_empty()
            .then(|| (self.space.clone(), self.rest_pieces_label()).into())
    }

    fn init_memo(&self, memo: &mut MemoToEnumerateCandidates) {
        memo.init(self.space.size());
    }

    fn rest_len(&self) -> usize {
        self.pieces.len()
    }

    fn rest_candidates_len(&self) -> usize {
        self.candidates.len()
    }

    fn fill_candidates(&mut self, solver: &mut Solver) {
        self.candidates = C::new(&self.space, &self.pieces, solver);
    }

    fn step(mut self, solver: &mut Solver) -> Result<Option<(Self, Option<Self>)>, Error> {
        let mut next_state = None;
        let mut it = self.candidates.into_iter();
        for candidate in it.by_ref() {
            tracing::trace!("try_place: candidate = {candidate:?}");

            if let Some(next_space) = self.space.take(candidate.piece.as_ref()) {
                if !self.pieces.contains_key(candidate.origin.as_ref()) {
                    return Err(Error::FailedRemovePiece);
                }

                let pieces: HashMap<_, _> = self
                    .pieces
                    .iter()
                    .filter(|(k, _)| *k != candidate.origin.as_ref())
                    .map(|(x1, x2)| (x1.clone(), x2.clone()))
                    .collect();

                next_state = Some((pieces, next_space));

                break;
            }
        }

        let (pieces, space) = match next_state {
            Some(space) => space,
            // 現行ステップにはめ込める候補がなかった
            None => {
                return Ok(None);
            }
        };

        self.candidates = it.collect::<Vec<_>>().into();

        if self.candidates.is_empty() {
            // 現行ステップにはめ込める候補がなかった
            return Ok(None);
        };

        // 空きマスが複数の領域に分断される候補は選ばない
        let mut sectors = None;
        if self.prunable_by_secter_splitted(&mut sectors, &space, &solver.strategy) {
            return Ok(Some((self, None)));
        }

        // 残りのピースの最小マス数分の空きマスグループがなかったら次ステップに入らないようにする
        if self.prunable_by_secter_size(&sectors, &pieces, &solver.strategy) {
            return Ok(Some((self, None)));
        }

        // 孤立セルがある場合は探索しなくて良いので次ステップに入らない
        if space.has_single_cell() {
            return Ok(Some((self, None)));
        }

        let mut new_state: Self = (space, pieces).into();
        new_state.fill_candidates(solver);

        // 候補の中に存在しないピースがある場合は次ステップに入らない
        if !new_state.has_next_pieces() {
            return Ok(Some((self, None)));
        }

        Ok(Some((self, Some(new_state))))
    }

    fn starting_log(&self) {
        tracing::info!("start: pieces = {:?}", self.rest_pieces_label());
        tracing::info!("start: placed = {:?}", self.placed());
    }

    fn remained_log(&self) {
        tracing::debug!(
            "remained: rest_candidates size = {}, rest_pieces = {:?}",
            self.candidates.len(),
            self.rest_pieces_label(),
        );
    }

    fn advanced_log(&self, next: &Self) {
        tracing::debug!(
            "advanced: space size = {}, rest_candidates size = {}, rest_pieces = {:?}",
            next.space().len(),
            self.rest_candidates_len(),
            next.rest_pieces_label(),
        );
    }

    fn fallback_log(prev: Option<&Self>) {
        tracing::debug!(
            "failed in step so back to previous state: rest_pieces = {:?}",
            prev.as_ref().map(|x| x.rest_pieces_label()),
        );
    }
}

impl From<(Space, Vec<&'static char>)> for Solved {
    fn from((space, rest_pieces_label): (Space, Vec<&'static char>)) -> Self {
        Self {
            space,
            rest_pieces_label,
        }
    }
}

impl SolvedState for Solved {
    fn hash(&self) -> BTreeSet<LabeledPoint> {
        self.space.placed().clone()
    }

    fn display(&self) {
        tracing::info!("hash = {:#08x}", self.space.hash_value());
        tracing::info!("positions = {:?}", self.space.placed());
        let display = StateDisplay::from(self.space.clone());
        for s in display.fmt().into_iter() {
            tracing::info!("{s}");
        }
        tracing::info!("rest_pieces = {:?}", self.rest_pieces_label);
    }
}

impl<C> ChoiceOneState<C>
where
    C: Candidates,
{
    /// 初期状態を作る。
    pub fn new(space: Space, pieces: Vec<Piece>) -> Self {
        let pieces = pieces
            .into_iter()
            .map(|piece| {
                let meta = PieceMetadata::new(piece.clone(), &space);
                (piece, meta)
            })
            .collect();

        Self {
            space,
            pieces,
            candidates: C::none(),
        }
    }

    fn space(&self) -> &Space {
        &self.space
    }

    fn placed(&self) -> &BTreeSet<LabeledPoint> {
        self.space.placed()
    }

    fn rest_pieces_label(&self) -> Vec<&'static char> {
        self.pieces.keys().map(|x| x.label()).collect()
    }

    fn prunable_by_secter_splitted(
        &self,
        sectors: &mut Option<Sectors>,
        space: &Space,
        strategy: &Strategy,
    ) -> bool {
        if !strategy.do_prune_by_secter_splitted(self.space.len()) {
            return false;
        }
        let ss = Sectors::from(space);
        let ret = ss.len() > 1;
        *sectors = Some(ss);
        ret
    }

    fn prunable_by_secter_size(
        &self,
        sectors: &Option<Sectors>,
        next_pieces: &HashMap<Piece, PieceMetadata>,
        strategy: &Strategy,
    ) -> bool {
        let calc_piece_min_len = || {
            next_pieces
                .iter()
                .map(|(x, _)| x.len())
                .min()
                .unwrap_or_default()
        };

        // 先に重い計算が終わってるので条件にかかわらず判定する
        if let Some(sectors) = sectors {
            let piece_min_len = calc_piece_min_len();
            return sectors.min_len() < piece_min_len;
        }

        if !strategy.do_prune_by_secter_size(self.space.len()) {
            return false;
        }

        let piece_min_len = calc_piece_min_len();

        let sectors = Sectors::from_points(self.space.iter().cloned().collect());

        sectors.min_len() < piece_min_len
    }

    fn has_next_pieces(&self) -> bool {
        self.pieces.iter().all(|(piece, _)| {
            self.candidates
                .iter()
                .any(|position| position.origin.as_ref() == piece)
        })
    }
}

impl<C> From<(Space, HashMap<Piece, PieceMetadata>)> for ChoiceOneState<C>
where
    C: Candidates,
{
    fn from((space, pieces): (Space, HashMap<Piece, PieceMetadata>)) -> Self {
        Self {
            space,
            pieces,
            candidates: C::none(),
        }
    }
}

impl<C> std::hash::Hash for ChoiceOneState<C>
where
    C: Candidates,
{
    fn hash<H>(&self, hasher: &mut H)
    where
        H: std::hash::Hasher,
    {
        self.space.hash(hasher);
        for piece in self.pieces.keys() {
            piece.hash(hasher);
        }
        self.space.placed().hash(hasher);
    }
}
