use crate::model::{
    AddOffset, LabeledPoint, MergedPiece, Piece, Point, PointCollection, Rotate, Sectors, Space,
};
use crate::solver::state::{SolvedState, State};
use crate::solver::MemoToEnumerateCandidates;
use crate::Error;
use crate::{Solver, StateDisplay};
use rand::seq::SliceRandom;
use std::collections::{BTreeMap, BTreeSet};
use std::sync::Arc;

pub struct MergedPieceState {
    bag: BTreeSet<MergedPiece>,
    space: Space,
    candidates: Vec<BTreeSet<MergedPiece>>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct EnabledMergedPlace {
    offset: Point,
    place: Arc<MergedPiece>,
}

pub struct Solved {
    piece: MergedPiece,
    size: i16,
}

impl State for MergedPieceState {
    type Solved = Solved;

    fn solved(&self) -> Option<Self::Solved> {
        for mp in self.bag.iter() {
            if mp.len() == self.space.len() {
                return Some(Solved {
                    piece: mp.clone(),
                    size: self.space.size(),
                });
            }
        }
        None
    }

    fn rest_len(&self) -> usize {
        self.bag.len()
    }

    fn init_memo(&self, memo: &mut MemoToEnumerateCandidates) {
        memo.init(self.space.size());
    }

    fn rest_candidates_len(&self) -> usize {
        self.candidates.len()
    }

    fn fill_candidates(&mut self, solver: &mut Solver) {
        self.candidates = self.gen(solver);
    }

    fn step(mut self, solver: &mut Solver) -> Result<Option<(Self, Option<Self>)>, Error>
    where
        Self: Sized,
    {
        if self.candidates.is_empty() {
            return Ok(None);
        }

        let mut it = self.candidates.into_iter();
        let mut next_state = Self {
            bag: it.next().unwrap(), // 上で is_empty() してる
            space: self.space.clone(),
            candidates: vec![],
        };

        self.candidates = it.collect::<Vec<_>>();

        next_state.fill_candidates(solver);

        Ok(Some((self, Some(next_state))))
    }

    fn starting_log(&self) {
        let bag_strs = self.bag_strs();
        tracing::info!("start: pieces = {bag_strs:?}");
    }

    fn remained_log(&self) {
        tracing::debug!(
            "remained: rest_candidates size = {}, piece_in_bag = {:?}",
            self.candidates.len(),
            self.bag_strs(),
        );
    }

    fn advanced_log(&self, next: &Self) {
        tracing::debug!(
            "advanced: rest_candidates size = {}, piece_in_bag = {:?}",
            self.rest_candidates_len(),
            next.bag_strs(),
        );
    }

    fn fallback_log(prev: Option<&Self>) {
        tracing::debug!(
            "failed in step so back to previous state: piece_in_bag = {:?}",
            prev.as_ref().map(|x| x.bag_strs()),
        );
    }
}

impl SolvedState for Solved {
    fn hash(&self) -> BTreeSet<LabeledPoint> {
        self.piece.points().iter().cloned().collect()
    }

    fn display(&self) {
        let space = Space::new(self.size);
        let space = space.take(&self.piece).unwrap(); // unwrap できない状況で Solved を作らないと思われる
        tracing::info!("hash = {:#08x}", space.hash_value());
        tracing::info!("positions = {:?}", space.placed());
        let display = StateDisplay::from(space);
        for s in display.fmt().into_iter() {
            tracing::info!("{s}");
        }
        // tracing::info!("rest_pieces = {:?}", self.rest_pieces_label);
    }
}

impl MergedPieceState {
    pub fn new(space: Space, piece: Vec<Piece>) -> Self {
        let bag = piece.into_iter().map(MergedPiece::new).collect();

        Self {
            space,
            bag,
            candidates: vec![],
        }
    }

    fn bag_strs(&self) -> Vec<String> {
        self.bag.iter().map(|x| x.labels()).collect()
    }

    fn gen(&self, solver: &mut Solver) -> Vec<BTreeSet<MergedPiece>> {
        let mut ret = vec![];

        for idx0 in 0..self.bag.len() {
            for idx1 in idx0 + 1..self.bag.len() {
                let mut it = self.bag.clone().into_iter();
                let mut bag = BTreeSet::new();
                bag.extend(it.by_ref().take(idx0));
                let p0 = it.by_ref().next().unwrap(); // len まで回してるため
                bag.extend(it.by_ref().take(idx1 - idx0 - 1));
                let p1 = it.by_ref().next().unwrap(); // len まで回してるため
                bag.extend(it);

                let ps = self.merge_twe_pieces(&p0, &p1);
                for mp in ps.into_iter() {
                    let mut b = bag.clone();
                    b.insert(mp);
                    ret.push(b);
                }
            }
        }

        ret.shuffle(&mut solver.rng);
        ret
    }

    fn merge_twe_pieces(&self, lhs: &MergedPiece, rhs: &MergedPiece) -> Vec<MergedPiece> {
        let mut ret = BTreeSet::new();

        let lhs_enabled = generate_enabled(lhs.points(), &self.space);
        let rhs_enabled = generate_enabled(rhs.points(), &self.space);

        for mp1 in lhs_enabled.into_iter() {
            for mp2 in rhs_enabled.iter() {
                if mp1.conflict(mp2) || !mp1.has_neighbors(mp2, &self.space) {
                    continue;
                }

                let merged = mp1.place.as_ref().clone().merge(mp2.place.as_ref().clone());

                // 空きマスが複数の領域に分断される候補は選ばない
                if let Some(sp) = &self.space.take(&merged) {
                    if Sectors::from(sp).len() > 1 {
                        continue;
                    }
                } else {
                    continue;
                }

                ret.insert(merged);
            }
        }
        ret.into_iter().collect()
    }
}

fn generate_enabled(points: &BTreeSet<LabeledPoint>, space: &Space) -> Vec<EnabledMergedPlace> {
    let mut ret = BTreeMap::new();

    let r = points.rotate_set();
    for point in space.iter() {
        for rotated in r.iter() {
            let point_in_piece = rotated.first().unwrap().point(); // 元となる Piece は自動テストで 1 個あることを保証してる

            let offset = point.difference(point_in_piece);
            let place: BTreeSet<LabeledPoint> =
                rotated.iter().map(|x| x.add_offset(&offset)).collect();

            if !space.takable(&place) {
                continue;
            }

            let place: MergedPiece = place.into();
            let place = Arc::new(place.shift_near_origin());
            ret.insert(
                place.clone(),
                EnabledMergedPlace {
                    offset: point.clone(),
                    place,
                },
            );
        }
    }

    ret.into_values().collect()
}

impl EnabledMergedPlace {
    fn conflict(&self, rhs: &Self) -> bool {
        self.place
            .point_iter()
            .any(|mp1| rhs.place.point_iter().any(|mp2| mp1 == mp2))
    }

    fn has_neighbors(&self, rhs: &Self, space: &Space) -> bool {
        self.place
            .point_iter()
            .flat_map(|lp| lp.point().neighbors(space.size()))
            .filter(|p| self.place.point_iter().all(|lp| lp.point() != p))
            .any(|p1| rhs.place.point_iter().any(|mp2| p1 == *mp2.point()))
    }
}
